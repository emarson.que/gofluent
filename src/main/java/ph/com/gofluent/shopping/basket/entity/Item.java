package ph.com.gofluent.shopping.basket.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "ITEM")
public final class Item {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ITEM_ID")
    private Long itemId;

    @Column(name = "NAME")
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "QUANTITY")
    private int quantity;

    @Column(name = "PRICE")
    private Float price;

    public boolean isSameItem(final Item item) {
        return this.itemId.equals(item.getItemId())
            && this.name.equals(item.getName())
            && this.price.equals(item.getPrice())
            && this.description.equals(item.getDescription());
    }
}
