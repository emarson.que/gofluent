package ph.com.gofluent.shopping.basket.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Arrays;

@Configuration
@EnableSwagger2
public class SpringFoxConfig {
    @Bean
    public Docket apiDocket() {
        final ApiInfo info = new ApiInfo(
            "Shopping Basket Microservice",
            "APIs for Shopping Basket Microservice",
            "1.0.0",
            null,
            null,
            null,
            null,
            Arrays.asList()
        );
        return new Docket(DocumentationType.SWAGGER_2)
            .select()
            .apis(RequestHandlerSelectors.basePackage("ph.com.gofluent"))
            .paths(PathSelectors.any())
            .build()
            .apiInfo(info);
    }
}
