package ph.com.gofluent.shopping.basket.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ph.com.gofluent.shopping.basket.entity.Item;
import ph.com.gofluent.shopping.basket.entity.ShoppingBasket;
import ph.com.gofluent.shopping.basket.exception.BasketNotFoundException;
import ph.com.gofluent.shopping.basket.exception.CustomerNotFoundException;
import ph.com.gofluent.shopping.basket.exception.ItemNotFoundException;
import ph.com.gofluent.shopping.basket.repository.ItemRepository;
import ph.com.gofluent.shopping.basket.repository.ShoppingBasketRepository;
import ph.com.gofluent.shopping.basket.service.CustomerService;
import ph.com.gofluent.shopping.basket.service.ShoppingBasketService;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public final class ShoppingBasketServiceImpl implements ShoppingBasketService {
    private final ShoppingBasketRepository shoppingBasketRepository;
    private final CustomerService customerService;
    private final ItemRepository itemRepository;

    public ShoppingBasketServiceImpl(
        final ShoppingBasketRepository shoppingBasketRepository,
        final CustomerService customerService,
        final ItemRepository itemRepository
    ) {
        this.shoppingBasketRepository = shoppingBasketRepository;
        this.customerService = customerService;
        this.itemRepository = itemRepository;
    }

    /**
     * Creates a basket by customer
     * @param customerId
     * @return
     */
    @Override
    public ShoppingBasket createBasket(final Long customerId) throws CustomerNotFoundException {
        final ShoppingBasket shoppingBasket = new ShoppingBasket();
        shoppingBasket.setCustomer(this.customerService.findCustomerById(customerId));

        return this.shoppingBasketRepository.save(shoppingBasket);
    }

    /**
     * Adds an item in a basket
     * @param item
     * @param basketId
     * @param customerId
     */
    @Override
    public void addItem(
        final Item item,
        final Long basketId,
        final Long customerId
    ) throws CustomerNotFoundException, BasketNotFoundException {
        log.info("Retrieving shopping basket...");
        final Optional<ShoppingBasket> shoppingBasket = this.shoppingBasketRepository
            .findFirstByCustomerAndShoppingBasketId(this.customerService.findCustomerById(customerId), basketId);
        shoppingBasket.orElseThrow(BasketNotFoundException::new);
        log.info("Shopping basket available: {}", shoppingBasket.get());

        final List<Item> items = shoppingBasket.get().getItems();
        this.addQuantityOrItem(items, item);
        shoppingBasket.get().setItems(items);

        this.shoppingBasketRepository.save(shoppingBasket.get());

    }

    private void addQuantityOrItem(final List<Item> items, final Item addItem) {
        items
            .stream()
            .filter(item -> item.isSameItem(addItem))
            .findFirst()
            .ifPresentOrElse(
                item -> item.setQuantity(item.getQuantity() + addItem.getQuantity()),
                () -> {
                    addItem.setItemId(0L);
                    items.add(addItem);
                }
            );
    }

    /**
     * Removes an item in a basket
     * @param customerId
     * @param itemId
     * @param basketId
     */
    @Override
    public void removeItem(
        final Long customerId,
        final Long itemId,
        final Long basketId
    ) throws CustomerNotFoundException, BasketNotFoundException, ItemNotFoundException {
        log.info("Retrieving shopping basket...");
        final Optional<ShoppingBasket> shoppingBasket = this.shoppingBasketRepository
            .findFirstByCustomerAndShoppingBasketId(this.customerService.findCustomerById(customerId), basketId);
        shoppingBasket.orElseThrow(BasketNotFoundException::new);
        log.info("Shopping basket available: {}", shoppingBasket.get());

        if (shoppingBasket.get().getItems().removeIf(item -> item.getItemId().equals(itemId))) {
            this.itemRepository.delete(this.itemRepository.getOne(itemId));
        } else {
            throw new ItemNotFoundException();
        }
        this.shoppingBasketRepository.save(shoppingBasket.get());
    }

    /**
     * Gets all items in a basket
     * @param customerId
     * @param basketId
     * @return
     */
    @Override
    public List<Item> getItems(
        final Long customerId,
        final Long basketId
    ) throws CustomerNotFoundException, BasketNotFoundException {
        log.info("Retrieving shopping basket...");
        final Optional<ShoppingBasket> shoppingBasket = this.shoppingBasketRepository
            .findFirstByCustomerAndShoppingBasketId(this.customerService.findCustomerById(customerId), basketId);
        shoppingBasket.orElseThrow(BasketNotFoundException::new);
        log.info("Shopping basket available: {}", shoppingBasket.get());

        return shoppingBasket.get().getItems();
    }
}
