package ph.com.gofluent.shopping.basket.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ph.com.gofluent.shopping.basket.exception.ErrorCodeException;

@Data
@AllArgsConstructor
@NoArgsConstructor
public final class ErrorHandlerResponse {
    @JsonProperty("ErrorCode")
    private String errorCode;
    @JsonProperty("Message")
    private String message;

    public ErrorHandlerResponse(
        final ErrorCodeException exception,
        final String message
    ) {
        this.message = message;
        this.errorCode = exception.toCode();
    }

}
