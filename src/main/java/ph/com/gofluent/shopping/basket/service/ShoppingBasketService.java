package ph.com.gofluent.shopping.basket.service;

import ph.com.gofluent.shopping.basket.entity.Item;
import ph.com.gofluent.shopping.basket.entity.ShoppingBasket;
import ph.com.gofluent.shopping.basket.exception.BasketNotFoundException;
import ph.com.gofluent.shopping.basket.exception.CustomerNotFoundException;
import ph.com.gofluent.shopping.basket.exception.ItemNotFoundException;

import java.util.List;

/**
 * Shopping Basket Service
 */
public interface ShoppingBasketService {
    /**
     * Creates basket by customer
     * @param customerId
     * @return
     */
    ShoppingBasket createBasket(Long customerId) throws CustomerNotFoundException;

    /**
     * Adds item in a basket
     * @param item
     * @param basketId
     * @param customerId
     */
    void addItem(
        Item item,
        Long basketId,
        Long customerId
    ) throws CustomerNotFoundException, BasketNotFoundException;

    /**
     * Removes item in a basket
     * @param customerId
     * @param itemId
     * @param basketId
     */
    void removeItem(
        Long customerId,
        Long itemId,
        Long basketId
    ) throws CustomerNotFoundException, BasketNotFoundException, ItemNotFoundException;

    /**
     * Gets all items in a basket
     * @param customerId
     * @param basketId
     * @return
     */
    List<Item> getItems(Long customerId, Long basketId) throws CustomerNotFoundException, BasketNotFoundException;
}
