package ph.com.gofluent.shopping.basket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ph.com.gofluent.shopping.basket.entity.Item;

public interface ItemRepository extends JpaRepository<Item, Long> {
}
