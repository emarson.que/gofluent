package ph.com.gofluent.shopping.basket.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ph.com.gofluent.shopping.basket.controller.ShoppingBasketController;
import ph.com.gofluent.shopping.basket.dto.ErrorHandlerResponse;

@Slf4j
@ControllerAdvice(assignableTypes = {
    ShoppingBasketController.class
})
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
    private final HttpHeaders headers = new HttpHeaders();

    @ExceptionHandler({
        CustomerNotFoundException.class,
        BasketNotFoundException.class,
        ItemNotFoundException.class
    })
    public final ResponseEntity<ErrorHandlerResponse> handleBadRequestError(
        final ErrorCodeException ex,
        final HandlerMethod handlerMethod
    ) {
        headers.setContentType(MediaType.APPLICATION_JSON);
        final ErrorHandlerResponse errorResponse = new ErrorHandlerResponse(
            ex,
            ex.getMessage()
        );

        log.error("[BadRequestException] Handler: {} ErrorCode: {}, Message: {}",
            handlerMethod.getMethod().getDeclaringClass(),
            errorResponse.getErrorCode(), errorResponse.getMessage());
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<?> handleRuntimeExceptions(
        final Exception ex,
        final HandlerMethod handlerMethod
    ) {
        headers.setContentType(MediaType.TEXT_PLAIN);
        log.error("[Exception] Handler: {} Message: {}",
            handlerMethod.getMethod().getDeclaringClass(), ex.getMessage());
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Override
    protected final ResponseEntity<Object> handleMissingPathVariable(
        final MissingPathVariableException ex,
        final HttpHeaders headers,
        final HttpStatus status,
        final WebRequest request
    ) {
        log.error("[MissingPathVariable] Message: {} {}", status, ex.getMessage());
        return new ResponseEntity<>(ex.getMessage(), status);
    }

    @Override
    protected final ResponseEntity<Object> handleMissingServletRequestParameter(
        final MissingServletRequestParameterException ex,
        final HttpHeaders headers,
        final HttpStatus status,
        final WebRequest request
    ) {
        log.error("[MissingServletRequestParameter] Message: {} {}", status, ex.getMessage());
        return new ResponseEntity<>(ex.getMessage(), status);
    }
}
