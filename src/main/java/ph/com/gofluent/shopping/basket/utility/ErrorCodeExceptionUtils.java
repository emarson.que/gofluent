package ph.com.gofluent.shopping.basket.utility;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public final class ErrorCodeExceptionUtils {
    private static final String SHOPPING_BASKET_CODE = "S";
    private static final Map<String, Map<String, String>> BASKET = new ConcurrentHashMap<>();
    private static final Map<String, Map<String, String>> ERROR_CODES_RAW = new ConcurrentHashMap<>();
    public static final Map<String, Map<String, String>> ERROR_CODES = Collections
        .unmodifiableMap(ERROR_CODES_RAW);

    static {
        setBasketErrorCodes();
        mergeMap();
    }

    private ErrorCodeExceptionUtils() {
        // no args constructor
    }

    /**
     * Sets all error codes and messages mapping.
     */
    private static void setBasketErrorCodes() {
        final Map<String, String> errorCode = new ConcurrentHashMap<>();
        errorCode.put("000", "Internal Server Error.");
        errorCode.put("001", "Customer not found.");
        errorCode.put("002", "Shopping basket not found.");
        errorCode.put("003", "Item not found.");
        BASKET.put(SHOPPING_BASKET_CODE, errorCode);
    }

    private static void mergeMap() {
        ERROR_CODES_RAW.putAll(BASKET);
    }

}
