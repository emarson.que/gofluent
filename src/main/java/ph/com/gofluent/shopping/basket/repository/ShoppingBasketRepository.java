package ph.com.gofluent.shopping.basket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ph.com.gofluent.shopping.basket.entity.Customer;
import ph.com.gofluent.shopping.basket.entity.ShoppingBasket;

import java.util.Optional;

public interface ShoppingBasketRepository extends JpaRepository<ShoppingBasket, Long> {
    Optional<ShoppingBasket> findFirstByCustomerAndShoppingBasketId(Customer customer, Long basketId);
}
