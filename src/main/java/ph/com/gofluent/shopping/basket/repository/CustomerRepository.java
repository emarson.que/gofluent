package ph.com.gofluent.shopping.basket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ph.com.gofluent.shopping.basket.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
