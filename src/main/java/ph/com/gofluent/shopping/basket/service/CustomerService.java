package ph.com.gofluent.shopping.basket.service;

import ph.com.gofluent.shopping.basket.entity.Customer;
import ph.com.gofluent.shopping.basket.exception.CustomerNotFoundException;

public interface CustomerService {
    /**
     * Retrieve customer by id
     * @param customerId
     * @return
     */
    Customer findCustomerById(Long customerId) throws CustomerNotFoundException;
}
