package ph.com.gofluent.shopping.basket.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ph.com.gofluent.shopping.basket.entity.Customer;
import ph.com.gofluent.shopping.basket.exception.CustomerNotFoundException;
import ph.com.gofluent.shopping.basket.repository.CustomerRepository;
import ph.com.gofluent.shopping.basket.service.CustomerService;

import java.util.Optional;

@Slf4j
@Service
public final class CustomerServiceImpl implements CustomerService {
    private final CustomerRepository customerRepository;

    public CustomerServiceImpl(final CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public Customer findCustomerById(final Long customerId) throws CustomerNotFoundException {
        log.info("Checking for customer...");
        final Optional<Customer> customer = this.customerRepository.findById(customerId);
        customer.orElseThrow(CustomerNotFoundException::new);
        log.info("Retrieved customer: {}", customer.get());

        return customer.get();
    }
}
