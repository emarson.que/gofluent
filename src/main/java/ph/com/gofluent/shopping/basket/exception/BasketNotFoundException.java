package ph.com.gofluent.shopping.basket.exception;

import ph.com.gofluent.shopping.basket.utility.ErrorCodeExceptionUtils;

public class BasketNotFoundException extends ErrorCodeException {
    private static final String SHOPPING_BASKET_CODE = "S";
    private static final String CODE = "002";
    private static final String MESSAGE = ErrorCodeExceptionUtils.ERROR_CODES.get(SHOPPING_BASKET_CODE).get(CODE);

    public BasketNotFoundException() {
        super(
            CODE,
            MESSAGE
        );
    }
}
