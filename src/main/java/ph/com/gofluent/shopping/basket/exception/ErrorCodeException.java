package ph.com.gofluent.shopping.basket.exception;

public class ErrorCodeException extends Exception {
    private static final String SHOPPING_BASKET_CODE = "S";
    private final String errorCode;

    public ErrorCodeException(
        final String errorCode,
        final String message
    ) {
        super(message);
        this.errorCode = errorCode;
    }

    public final String toCode() {
        return SHOPPING_BASKET_CODE + "." + this.errorCode;
    }
}
