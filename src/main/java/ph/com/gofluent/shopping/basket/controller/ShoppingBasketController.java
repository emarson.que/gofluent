package ph.com.gofluent.shopping.basket.controller;

import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ph.com.gofluent.shopping.basket.entity.Item;
import ph.com.gofluent.shopping.basket.entity.ShoppingBasket;
import ph.com.gofluent.shopping.basket.exception.BasketNotFoundException;
import ph.com.gofluent.shopping.basket.exception.CustomerNotFoundException;
import ph.com.gofluent.shopping.basket.exception.ItemNotFoundException;
import ph.com.gofluent.shopping.basket.service.ShoppingBasketService;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/customers/{customerId}/baskets")
public final class ShoppingBasketController {
    private final ShoppingBasketService shoppingBasketService;

    public ShoppingBasketController(final ShoppingBasketService shoppingBasketService) {
        this.shoppingBasketService = shoppingBasketService;
    }

    /**
     * POST
     * Basket Creation API
     * @param customerId
     * @return
     */
    @PostMapping
    public ResponseEntity<ShoppingBasket> createBasket(
        @ApiParam(defaultValue = "1")
        @PathVariable final Long customerId
    ) throws CustomerNotFoundException {
        log.info("Creating basket...");
        return ResponseEntity.ok(this.shoppingBasketService.createBasket(customerId));
    }

    /**
     * POST /{basketId}/items
     * Add an item to basket
     * @param customerId
     * @param basketId
     * @param item
     * @return
     * @throws CustomerNotFoundException
     * @throws BasketNotFoundException
     */
    @PostMapping("/{basketId}/items")
    public ResponseEntity<String> addItem(
        @PathVariable final Long customerId,
        @PathVariable final Long basketId,
        @RequestBody final Item item
    ) throws CustomerNotFoundException, BasketNotFoundException {
        log.info("Adding item...");
        this.shoppingBasketService.addItem(item, basketId, customerId);
        return ResponseEntity.ok("Item has been added to basket.");
    }

    /**
     * DELETE /{basketId}/items/{itemId}
     * Delete an item API
     *
     * @param customerId
     * @param basketId
     * @param itemId
     * @return
     * @throws ItemNotFoundException
     * @throws BasketNotFoundException
     * @throws CustomerNotFoundException
     */
    @DeleteMapping("/{basketId}/items/{itemId}")
    public ResponseEntity<String> removeItem(
        @PathVariable final Long customerId,
        @PathVariable final Long basketId,
        @PathVariable final Long itemId
    ) throws ItemNotFoundException, BasketNotFoundException, CustomerNotFoundException {
        log.info("Removing item...");
        this.shoppingBasketService.removeItem(customerId, itemId, basketId);
        return ResponseEntity.ok("Item has been removed from basket.");
    }

    /**
     * GET /{basketId}/items
     * Retrieve items in basket
     *
     * @param customerId
     * @param basketId
     * @return
     * @throws CustomerNotFoundException
     * @throws BasketNotFoundException
     */
    @GetMapping("/{basketId}/items")
    public ResponseEntity<List<Item>> getItems(
        @PathVariable final Long customerId,
        @PathVariable final Long basketId
    ) throws CustomerNotFoundException, BasketNotFoundException {
        log.info("Retrieving items...");
        return ResponseEntity.ok(this.shoppingBasketService.getItems(customerId, basketId));
    }
}
