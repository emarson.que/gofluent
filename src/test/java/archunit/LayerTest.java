package archunit;

import archunit.common.ScopeClasses;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.library.Architectures.layeredArchitecture;

public class LayerTest {
    @Test
    public void repositoryUsage() {
        layeredArchitecture()
            .layer("Controller").definedBy("..controller..")
            .layer("Service").definedBy("..service..")
            .layer("Workflow").definedBy("..workflow..")
            .layer("Repository").definedBy("..repository..")
            .layer("Lambda").definedBy("..lambda..")
            .layer("Factory").definedBy("..factory..")
            .layer("Jobs").definedBy("..jobs..")

            .whereLayer("Controller").mayNotBeAccessedByAnyLayer()
            .whereLayer("Service").mayOnlyBeAccessedByLayers("Controller", "Workflow", "Lambda", "Factory", "Jobs")
            .whereLayer("Repository").mayOnlyBeAccessedByLayers("Service", "Workflow", "Lambda", "Factory")
            .check(ScopeClasses.CLASSES);
    }
}
