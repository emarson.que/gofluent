package com.peleven.transactions.services.implementations;

/*
import com.peleven.transactions.dto.PageableResponseDTO;
import com.peleven.transactions.dto.SuggestedBucketCreateRequestDTO;
import com.peleven.transactions.dto.SuggestedBucketUpdateRequestDTO;
import com.peleven.transactions.exceptions.BaseServiceException;
import com.peleven.transactions.exceptions.SuggestedBucketNotFoundException;
import com.peleven.transactions.models.SuggestedBucket;
import com.peleven.transactions.repositories.SuggestedBucketRepository;
import com.peleven.transactions.utilities.PageableUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.everyItem;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
@Slf4j
public class SuggestedBucketServiceImplTest {
    @Mock
    private SuggestedBucketRepository repository;

    private SuggestedBucketServiceImpl suggestedBucketService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        suggestedBucketService = new SuggestedBucketServiceImpl(repository);
    }

    @Test(expected = SuggestedBucketNotFoundException.class)
    public void whenGetSuggestedBucketById_isNull_thenThrowException() throws BaseServiceException {
        when(this.repository.findById(any())).thenReturn(Optional.empty());

        this.suggestedBucketService.getSuggestedBucketById(UUID.randomUUID());

        verify(repository).findById(any());
    }

    @Test
    public void whenGetSuggestedBucketById_exists_thenReturnSuccess() throws BaseServiceException {
        final SuggestedBucket expectedResponse = this.preFillSuggestedBucket();
        when(this.repository.findById(any())).thenReturn(Optional.of(expectedResponse));

        final SuggestedBucket actualResponse = this.suggestedBucketService.getSuggestedBucketById(UUID.randomUUID());

        assertEquals(expectedResponse.getName(), actualResponse.getName());
        assertEquals(expectedResponse.getDescription(), actualResponse.getDescription());
        assertEquals(expectedResponse.getDetailsLink(), actualResponse.getDetailsLink());
        assertTrue(actualResponse.getIsShown());
        assertEquals(expectedResponse.getCreatedAt(), actualResponse.getCreatedAt());

        verify(repository).findById(any());
    }

    @Test
    public void whenGetSuggestedBuckets_isNull_thenReturnEmptyResponse() {
        when(this.repository.findAllByIsShownIsTrue(any())).thenReturn(Page.empty());

        PageableResponseDTO<SuggestedBucket> actualResponse = this.suggestedBucketService
                .getSuggestedBuckets(Pageable.unpaged());

        assertNull(actualResponse.getData());
        assertEquals(Integer.valueOf(0), actualResponse.getPage());
        assertEquals(Integer.valueOf(0), actualResponse.getSize());
        assertEquals(Integer.valueOf(0), actualResponse.getTotalPages());
        assertEquals(Long.valueOf(0L), actualResponse.getTotalElements());

        verify(repository).findAllByIsShownIsTrue(any());
    }

    @Test
    public void whenGetSuggestedBuckets_exist_thenReturnList() {
        final SuggestedBucket expectedResponse = this.preFillSuggestedBucket();

        final Pageable paging = PageableUtil.setPaging(0, 10, "desc");

        when(this.repository.findAllByIsShownIsTrue(any()))
            .thenReturn(new PageImpl<>(Collections.singletonList(expectedResponse)));

        final PageableResponseDTO<SuggestedBucket> actualResponse = this.suggestedBucketService
                .getSuggestedBuckets(paging);

        assertThat(actualResponse.getData(), everyItem(hasProperty("name",
                is(expectedResponse.getName()))));
        assertThat(actualResponse.getData(), everyItem(hasProperty("description",
                is(expectedResponse.getDescription()))));
        assertThat(actualResponse.getData(), everyItem(hasProperty("id",
                is(expectedResponse.getId()))));
        assertThat(actualResponse.getData(), everyItem(hasProperty("detailsLink",
                is(expectedResponse.getDetailsLink()))));
        assertThat(actualResponse.getData(), everyItem(hasProperty("isShown",
                is(expectedResponse.getIsShown()))));
        assertThat(actualResponse.getData(), everyItem(hasProperty("createdAt",
                is(expectedResponse.getCreatedAt()))));

        assertEquals(Integer.valueOf(0), actualResponse.getPage());
        assertEquals(Integer.valueOf(0), actualResponse.getSize());
        assertEquals(Integer.valueOf(1), actualResponse.getTotalPages());
        assertEquals(Long.valueOf(1L), actualResponse.getTotalElements());

        verify(repository).findAllByIsShownIsTrue(any());
    }

    @Test
    public void whenCreateSuggestBucket_isValid_thenReturnSuccess() {
        final SuggestedBucketCreateRequestDTO requestDTO = new SuggestedBucketCreateRequestDTO(
                "newName", "newDesc", "newLink");
        when(repository.save(any())).thenReturn(new SuggestedBucket(requestDTO));

        SuggestedBucket actualResponse = this.suggestedBucketService.createSuggestedBucket(requestDTO);
        assertEquals(requestDTO.getName(), actualResponse.getName());
        assertEquals(requestDTO.getDescription(), actualResponse.getDescription());
        assertEquals(requestDTO.getDetailsLink(), actualResponse.getDetailsLink());

        verify(repository).save(any());
    }


    @Test(expected = SuggestedBucketNotFoundException.class)
    public void whenUpdateSuggestBucket_suggestedBuckedNotFound_thenThrowException() throws BaseServiceException {
        when(this.repository.findById(any())).thenReturn(Optional.empty());

        this.suggestedBucketService.updateSuggestedBucket(new SuggestedBucketUpdateRequestDTO());

        verify(repository).findById(any());
        verifyZeroInteractions(repository.save(any()));
    }

    @Test
    public void whenUpdateSuggestBucket_withUpdateOneField_thenReturnSuccessWithOneUpdatedField()
            throws BaseServiceException {
        final SuggestedBucket existingSuggestedBucket = this.preFillSuggestedBucket();
        final SuggestedBucket updatedSuggestedBucket = this.preFillSuggestedBucket();
        updatedSuggestedBucket.setIsShown(false);

        when(this.repository.findById(any())).thenReturn(Optional.of(existingSuggestedBucket));
        when(this.repository.save(any())).thenReturn(updatedSuggestedBucket);

        final SuggestedBucket actualResponse = this.suggestedBucketService
            .updateSuggestedBucket(new SuggestedBucketUpdateRequestDTO(
                existingSuggestedBucket.getId(), null, null, null, false));

        assertEquals(existingSuggestedBucket.getName(), actualResponse.getName());
        assertEquals(existingSuggestedBucket.getDescription(), actualResponse.getDescription());
        assertEquals(existingSuggestedBucket.getDetailsLink(), actualResponse.getDetailsLink());
        assertEquals(updatedSuggestedBucket.getIsShown(), actualResponse.getIsShown());

        verify(repository).findById(any());
        verify(repository).save(any());
    }


    @Test
    public void whenUpdateSuggestBucket_withUpdateAllFields_thenReturnSuccessWithAllUpdatedField()
            throws BaseServiceException {
        final SuggestedBucket existingSuggestedBucket = this.preFillSuggestedBucket();
        final SuggestedBucket updatedSuggestedBucket = new SuggestedBucket();
        updatedSuggestedBucket.setName("updatedName");
        updatedSuggestedBucket.setDescription("updatedDesc");
        updatedSuggestedBucket.setDetailsLink("updatedLink");
        updatedSuggestedBucket.setIsShown(false);

        when(this.repository.findById(any())).thenReturn(Optional.of(existingSuggestedBucket));
        when(this.repository.save(any())).thenReturn(updatedSuggestedBucket);

        final SuggestedBucket actualResponse = this.suggestedBucketService
                .updateSuggestedBucket(new SuggestedBucketUpdateRequestDTO(
                    existingSuggestedBucket.getId(), updatedSuggestedBucket.getName(),
                    updatedSuggestedBucket.getDescription(), updatedSuggestedBucket.getDetailsLink(),
                    updatedSuggestedBucket.getIsShown()));

        assertEquals(updatedSuggestedBucket.getName(), actualResponse.getName());
        assertEquals(updatedSuggestedBucket.getDescription(), actualResponse.getDescription());
        assertEquals(updatedSuggestedBucket.getDetailsLink(), actualResponse.getDetailsLink());
        assertEquals(updatedSuggestedBucket.getIsShown(), actualResponse.getIsShown());

        verify(repository).findById(any());
        verify(repository).save(any());
    }

    @Test(expected = SuggestedBucketNotFoundException.class)
    public void wheDeleteSuggestedBucket_bucketDoesNotExists_thenThrowException() throws BaseServiceException {
        when(this.repository.findById(any())).thenReturn(Optional.empty());

        this.suggestedBucketService.deleteSuggestedBucket(UUID.randomUUID());

        verify(repository).findById(any());
        verifyNoMoreInteractions(repository);
    }

    @Test
    public void whenDeleteSuggestedBucket_exists_thenReturnSuccess() throws BaseServiceException {
        final SuggestedBucket expectedResponse = this.preFillSuggestedBucket();
        when(this.repository.findById(any())).thenReturn(Optional.of(expectedResponse));
        doNothing().when(this.repository).delete(any());

        final String actualResponse = this.suggestedBucketService.deleteSuggestedBucket(UUID.randomUUID());

        assertEquals("Successfully deleted suggested bucket", actualResponse);

        verify(repository).findById(any());
        verify(repository).delete(any());
    }

    private SuggestedBucket preFillSuggestedBucket() {
        SuggestedBucket suggestedBucket = new SuggestedBucket();
        suggestedBucket.setId(UUID.randomUUID());
        suggestedBucket.setName("Sample Name");
        suggestedBucket.setDescription("Description");
        suggestedBucket.setDetailsLink("Details link");
        suggestedBucket.setIsShown(true);
        suggestedBucket.setCreatedAt(LocalDateTime.of(
                2020, Month.AUGUST, 1, 0 , 0 ,0));
        return suggestedBucket;
    }
}

 */
