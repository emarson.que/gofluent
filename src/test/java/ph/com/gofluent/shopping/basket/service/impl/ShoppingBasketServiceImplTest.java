package ph.com.gofluent.shopping.basket.service.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import ph.com.gofluent.shopping.basket.entity.Customer;
import ph.com.gofluent.shopping.basket.entity.Item;
import ph.com.gofluent.shopping.basket.entity.ShoppingBasket;
import ph.com.gofluent.shopping.basket.exception.BasketNotFoundException;
import ph.com.gofluent.shopping.basket.exception.CustomerNotFoundException;
import ph.com.gofluent.shopping.basket.exception.ItemNotFoundException;
import ph.com.gofluent.shopping.basket.repository.ItemRepository;
import ph.com.gofluent.shopping.basket.repository.ShoppingBasketRepository;
import ph.com.gofluent.shopping.basket.service.CustomerService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ShoppingBasketServiceImplTest {
    @Mock
    CustomerService customerService;

    @Mock
    private ItemRepository itemRepository;

    @Mock
    private ShoppingBasketRepository shoppingBasketRepository;

    private ShoppingBasketServiceImpl shoppingBasketService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        shoppingBasketService = new ShoppingBasketServiceImpl(
            shoppingBasketRepository, customerService, itemRepository);
    }

    @Test
    void createBasketCustomerNotFoundTest() throws CustomerNotFoundException {
        when(this.customerService.findCustomerById(any())).thenThrow(CustomerNotFoundException.class);
        assertThrows(CustomerNotFoundException.class,
            () -> this.shoppingBasketService.createBasket(anyLong()));
    }

    @Test
    void createBasketTest() throws CustomerNotFoundException {
        final Customer customer = getCustomer();
        final ShoppingBasket expectedResponse = getShoppingBasket(customer);

        when(this.customerService.findCustomerById(any())).thenReturn(new Customer());
        when(this.shoppingBasketRepository.save(any())).thenReturn(expectedResponse);

        assertEquals(expectedResponse, this.shoppingBasketService.createBasket(anyLong()));
    }

    private ShoppingBasket getShoppingBasket(final Customer customer) {
        final ShoppingBasket expectedResponse = new ShoppingBasket();
        expectedResponse.setCustomer(customer);
        final List<Item> items = new ArrayList<>();
        final Item item = new Item();
        item.setItemId(1L);
        items.add(item);
        expectedResponse.setItems(items);
        return expectedResponse;
    }

    private Customer getCustomer() {
        final Customer customer = new Customer();
        customer.setCustomerId(1L);
        customer.setName("Emarson Que");
        return customer;
    }

    @Test
    void addItem() throws CustomerNotFoundException, BasketNotFoundException {
        final Customer customer = getCustomer();
        final ShoppingBasket expectedResponse = getShoppingBasket(customer);
        when(this.customerService.findCustomerById(anyLong())).thenReturn(new Customer());
        when(this.shoppingBasketRepository.save(any())).thenReturn(expectedResponse);
        when(this.shoppingBasketRepository.findFirstByCustomerAndShoppingBasketId(
            any(), anyLong())).thenReturn(Optional.of(expectedResponse));

        final Item item = new Item();
        this.shoppingBasketService.addItem(item, 1L, 1L);
        verify(this.shoppingBasketRepository, times(1)).save(any());
    }

    @Test
    void removeItem() throws ItemNotFoundException, BasketNotFoundException, CustomerNotFoundException {
        final Customer customer = getCustomer();
        final ShoppingBasket expectedResponse = getShoppingBasket(customer);
        final Item item = new Item();
        item.setItemId(1L);
        doNothing().when(this.itemRepository).delete(item);
        when(this.customerService.findCustomerById(anyLong())).thenReturn(new Customer());
        when(this.shoppingBasketRepository.save(any())).thenReturn(expectedResponse);
        when(this.itemRepository.getOne(anyLong())).thenReturn(item);
        when(this.shoppingBasketRepository.findFirstByCustomerAndShoppingBasketId(
            any(), anyLong())).thenReturn(Optional.of(expectedResponse));

        this.shoppingBasketService.removeItem(1L, 1L, 1L);
        verify(this.shoppingBasketRepository, times(1)).save(any());
    }

    @Test
    void getItems() throws CustomerNotFoundException, BasketNotFoundException {
        final Customer customer = getCustomer();
        final ShoppingBasket expectedResponse = getShoppingBasket(customer);
        when(this.customerService.findCustomerById(anyLong())).thenReturn(new Customer());
        when(this.shoppingBasketRepository.findFirstByCustomerAndShoppingBasketId(
            any(), anyLong())).thenReturn(Optional.of(expectedResponse));

        assertEquals(expectedResponse.getItems(), this.shoppingBasketService.getItems(1l, 1l));
    }
}